provider "aws" {
  region = "us-east-1"
}
module "bals_vpc" {
    source = "./modules/vpc"
    region = "us-east-1"
    module_tags = {purpose= "studing" , created_by = "terraform"}
    cidr_range = "10.3.0.0/16"
    public_subnet_cidr = ["10.3.1.0/24","10.3.2.0/24"]
    private_subnet_cidr = ["10.3.11.0/24","10.3.12.0/24"]
    availability_zone = ["us-east-1a","us-east-1b"]
}
