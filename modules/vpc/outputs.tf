output "public_subnet_id" {
  value = aws_subnet.public_subnet.*.id
}
output "ami_id" {
  value = data.aws_ssm_parameter.prometheus.value
}


output "private_subnet_id" {
  value = aws_subnet.private_subnet.*.id
}

output "security_grp_id" {
  value = aws_security_group.allow_tls.*.id
}

output "vpc_id" {
  value = aws_vpc.this.id
}
