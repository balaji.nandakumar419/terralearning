variable "region" {
  type    = string
  default = "us-east-1"
}
variable "module_tags" {
  type = map(any)

}

variable "cidr_range"{ 
 type = string
}

variable "public_subnet_cidr" {
  type    = list(string)
}

variable "private_subnet_cidr" {
  type    = list(string)
}

variable "availability_zone" {
  type = list(string)
}
