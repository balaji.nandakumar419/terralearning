resource "aws_vpc" "this" {
  cidr_block = var.cidr_range
  tags       = var.module_tags
}


resource "aws_internet_gateway" "this_igw" {
  vpc_id = aws_vpc.this.id
  tags = var.module_tags

}

resource "aws_subnet" "public_subnet" {
  count = length(var.public_subnet_cidr)
  vpc_id                  = aws_vpc.this.id
  cidr_block              = element(var.public_subnet_cidr,count.index)
  map_public_ip_on_launch = true
  tags                    = merge(var.module_tags,{Name="public_subnet_${count.index}"})
  availability_zone = element(var.availability_zone,count.index)
}

resource "aws_subnet" "private_subnet" {
  count = length(var.private_subnet_cidr)
  vpc_id                  = aws_vpc.this.id
  cidr_block              = element(var.private_subnet_cidr,count.index)
  tags                    = merge(var.module_tags,{Name="private_subnet_${count.index}"})
  availability_zone = element(var.availability_zone,count.index)
}




data "aws_ssm_parameter" "prometheus" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.this.id
}


#Public Route
resource "aws_route_table" "public" {
  count = length(var.public_subnet_cidr) > 0 ? 1 : 0
  vpc_id = aws_vpc.this.id
  tags =var.module_tags
}

#public associate
resource "aws_route_table_association" "public_route_associate" {
  count = length(var.public_subnet_cidr)
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public[0].id
}

#Public IG route
resource "aws_route" "public_internet_gateway" {
  count =  length(var.public_subnet_cidr) > 0 ? 1 : 0

  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this_igw.id

  timeouts {
    create = "5m"
  }
}

#Private Route table
resource "aws_route_table" "private" {
  count = length(var.private_subnet_cidr) > 0 ? 1 : 0
  vpc_id = aws_vpc.this.id
  tags =var.module_tags
}

#NAT EIP
resource "aws_eip" "nat_eip" {
 depends_on = [aws_internet_gateway.this_igw]
  vpc      = true
}


#private nat route
resource "aws_route" "private_nat_gateway" {
  count =  length(var.public_subnet_cidr) > 0 ? 1 : 0
  route_table_id         = aws_route_table.private[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.aws_nat_gateway[0].id

  timeouts {
    create = "5m"
  }
}

#private associate
resource "aws_route_table_association" "private_route_associate" {
  count = length(var.private_subnet_cidr)
  subnet_id      =  "${element(aws_subnet.private_subnet.*.id, count.index)}"
  route_table_id = aws_route_table.private[0].id
}


#NAT resource
resource "aws_nat_gateway" "aws_nat_gateway" {
  count =  length(var.public_subnet_cidr) > 0 ? 1 : 0
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet[0].id
  tags = var.module_tags
  depends_on = [aws_internet_gateway.this_igw]
}
